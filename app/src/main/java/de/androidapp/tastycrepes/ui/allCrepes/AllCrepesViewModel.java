package de.androidapp.tastycrepes.ui.allCrepes;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AllCrepesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AllCrepesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("See all the delicious crepes on our menu.");
    }

    public LiveData<String> getText() {
        return mText;
    }
}