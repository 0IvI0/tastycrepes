package de.androidapp.tastycrepes.ui.theCrepeTeam;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TheCrepeTeamViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TheCrepeTeamViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is The Crepe Team fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}