package de.androidapp.tastycrepes.ui.theCrepeTeam;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import de.androidapp.tastycrepes.R;

public class TheCrepeTeamFragment extends Fragment {

    private TheCrepeTeamViewModel theCrepeTeamViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        theCrepeTeamViewModel =
                ViewModelProviders.of(this).get(TheCrepeTeamViewModel.class);
        View root = inflater.inflate(R.layout.fragment_the_crepe_team, container, false);
        final TextView textView = root.findViewById(R.id.text_the_crepe_team);
        theCrepeTeamViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}