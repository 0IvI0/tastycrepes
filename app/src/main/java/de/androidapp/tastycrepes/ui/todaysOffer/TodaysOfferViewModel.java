package de.androidapp.tastycrepes.ui.todaysOffer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TodaysOfferViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TodaysOfferViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Today's offer fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}