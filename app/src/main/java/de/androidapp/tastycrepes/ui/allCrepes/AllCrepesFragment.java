package de.androidapp.tastycrepes.ui.allCrepes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import de.androidapp.tastycrepes.R;

public class AllCrepesFragment extends Fragment {

    private AllCrepesViewModel allCrepesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        allCrepesViewModel =
                ViewModelProviders.of(this).get(AllCrepesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_all_crepes, container, false);
        final TextView textView = root.findViewById(R.id.text_all_crepes);
        allCrepesViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}