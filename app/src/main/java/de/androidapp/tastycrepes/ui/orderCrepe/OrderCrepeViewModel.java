package de.androidapp.tastycrepes.ui.orderCrepe;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class OrderCrepeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public OrderCrepeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Order your meal!");
    }

    public LiveData<String> getText() {
        return mText;
    }
}