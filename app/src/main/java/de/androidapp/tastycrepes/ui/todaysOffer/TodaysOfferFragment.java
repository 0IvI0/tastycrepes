package de.androidapp.tastycrepes.ui.todaysOffer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import de.androidapp.tastycrepes.R;

public class TodaysOfferFragment extends Fragment {

    private TodaysOfferViewModel todaysOfferViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        todaysOfferViewModel =
                ViewModelProviders.of(this).get(TodaysOfferViewModel.class);
        View root = inflater.inflate(R.layout.fragment_todays_offer, container, false);
        final TextView textView = root.findViewById(R.id.text_todays_offer);
        todaysOfferViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}