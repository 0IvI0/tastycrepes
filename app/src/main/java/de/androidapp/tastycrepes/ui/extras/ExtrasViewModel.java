package de.androidapp.tastycrepes.ui.extras;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ExtrasViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ExtrasViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Extras fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}