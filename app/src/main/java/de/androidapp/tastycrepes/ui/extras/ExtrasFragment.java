package de.androidapp.tastycrepes.ui.extras;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import de.androidapp.tastycrepes.R;

public class ExtrasFragment extends Fragment {

    private ExtrasViewModel extrasViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        extrasViewModel =
                ViewModelProviders.of(this).get(ExtrasViewModel.class);
        View root = inflater.inflate(R.layout.fragment_extras, container, false);
        final TextView textView = root.findViewById(R.id.text_extras);
        extrasViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}